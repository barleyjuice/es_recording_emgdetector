# EMG Detector recording plugin for Experiment Suite Core #

Additional module for EMG-signal recording using data acquisition channel based on set of Arduino-connected EMG Detectors. The module connects to channel, acquires data and writes them into csv log file. Optionally, any information about stimuli event occurence can be also added in a log within `marker_channel` column. Currently there can be only one event channel.

## Hardware requirements ##

1. Module interconnects with EMG data channel via COM-port (which is required to set via constructor of `EmgDetectorRecorder` class): for Arduino-based channel it is possible to connect either by USB or by Bluetooth adapter.

2. Data from module must be transmitted in a special format: each data sample must contain comma-separated integer values of corresponding EMG amplitude (where each value corresponds to specific EMG channel) without extra spaces.

The current version is tesded with following configuration which is proved to be correct:

*  Arduino Uno as a main board of the channel;
*  Grove EMG Detector by SeeedStudio x2, connected to Arduino using Grove Base Shield;
*  Arduino collects from EMG Detectors and sends data samples with following format: `data_1,data_2`, where `data_1` and `data_2` are amplitude values from the corresponding EMG Detector.

## Installation ##

To install the module as dependency in your Python project simply type:
```
pip install git+https://barleyjuice@bitbucket.org/barleyjuice/es_recording_emgdetector.git#egg=es_recording_emgdetector
```
If the installation was successful, you will be able to import the module in your project:
```python
from es_recording.emgdetector import EmgDetectorRecorder
```

## Use ##

Firstly create `EmgDetectorRecorder` object and pass in constructor `marker_channel` parameter - name of the additional column in recording log where information about event occurence will be placed; and `com_port` parameter - name of the serial port to which the EMG device is connected to:

```python
recorder = EmgDetectorRecorder(
    marker_channel="EVENT_MARKER",
    com_port="COM17")
```

There are also optional parameters:

*  `n_channels` - amount of EMG data channels. Defaults to 1;
*  `channel_names` - EMG data channels' names. If not provided, names will be chosen by default (CH_ + index): CH_0, CH_1, ... etc. Defaults to [];
*  `baudrate` - serial port baudrate. Defaults to 9600;
*  `read_period_seconds` - data reading period in seconds. Defaults to 0.1.

Then the above constructed `EmgDetectorRecorder` object in `recorder` variable can be passed as input parameter to create `Application` instance:

```python
# assume that `experimentScheme` and `presenter` are constructed avove
app = Application(
    presenter=presenter, 
    experimentScheme=experimentScheme, 
    recorder=recorder)
```
