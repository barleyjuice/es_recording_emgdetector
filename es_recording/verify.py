from emgdetector import EmgDetectorRecorder

recorder = EmgDetectorRecorder(marker_channel="MARKER", com_port="COM17", read_period_seconds=1, n_channels=2, channel_names=["First"])
recorder.enable()
recorder.start_recording()