from distutils.log import warn
from time import time_ns
import serial
import re

from es_core.recording import Recorder

class ReadLine:
    def __init__(self, s):
        self.buf = bytearray()
        self.s = s
    
    def readline(self):
        i = self.buf.find(b"\n")
        # print(f"Buf state: {self.buf}")
        if i >= 0:
            r = self.buf[:i+1]
            self.buf = self.buf[i+1:]
            # print(f"Ret: {r}, buf: {self.buf}")
            return r
        while True:
            i = max(1, self.s.in_waiting)
            # i = max(1, min(4 - len(self.buf), self.s.in_waiting))
            # i = max(1, min(2048, self.s.in_waiting))
            # print(f"Reading {i}")
            # start_read = time_ns()
            data = self.s.read(i)
            # elapsed_read = start_read - time_ns()
            # print(f"New data: {data}, elapsed: {elapsed_read}")
            i = data.find(b"\n")
            if i >= 0:
                r = self.buf + data[:i+1]
                self.buf[0:] = data[i+1:]
                # print(f"Ret: {r}, buf: {self.buf}, elapsed: {start_read - time_ns()}")
                return r
            else:
                # start_ext = time_ns()
                self.buf.extend(data)
                # print(f"Buf upd: {self.buf}, elapsed: {start_ext - time_ns()}")


class EmgDetectorRecorder(Recorder):

    def __init__(self, marker_channel, com_port, n_channels=1, channel_names=[], baudrate=9600, read_period_seconds=0.1, profiling_mode=False) -> None:
        """Collects and logs EMG data from serial port

        Args:
            marker_channel (string): name of the additional column in recording log where information about event occurence will be placed
            com_port (string): serial port name, to which the device is connected to
            n_channels (int, optional): amount of EMG data channels. Defaults to 1.
            channel_names (list, optional): EMG data channels' names. If not provided, names will be chosen by default. Defaults to [].
            baudrate (int, optional): serial port baudrate. Defaults to 9600.
            read_period_seconds (float, optional): data reading period in seconds. Defaults to 0.1.

        Raises:
            RuntimeError: when number of data channels is less than 1
        """
        self.serial = serial.Serial(com_port, baudrate)
        self.reader = ReadLine(self.serial)
        if n_channels < 1:
            raise RuntimeError("Number of channels must be greater than 0!")

        self.channels = channel_names

        if n_channels > len(channel_names):
            warn("Channel names are not fully specified. They will be named with some default values.")
            for i in range(n_channels - (n_channels-len(channel_names)), n_channels):
                self.channels.append(f"CH_{i}")
        
        print(f"Channels: {self.channels}")

        self._read_period_seconds = read_period_seconds

        self._sig_entries_regexp = "(" + ",".join(["[0-9]+" for i in range(0, n_channels)]) + ")"

        if marker_channel:
            self.channels.append(marker_channel)

        self.channels.append("TIMESTAMP")

        self.profiling_mode = profiling_mode
        super().__init__(channels=self.channels, marker_channel=marker_channel)

    def log_data(self):
        last_loop_begin = time_ns()
        while True:
            if not self.enabled:
                break
            start = time_ns()
            if self.profiling_mode:
                print(f"Elapsed sinse last iter: {start - last_loop_begin}")
            last_loop_begin = start
            try:
                
                data = self._read_emg_once()

            except Exception as e:
                print(e)
                continue
            if not data:
                warn("Data is not fetched this time")
                continue
            current_event_cnt = self.event_cnt
            data_write = [str(sample) for (i, sample) in enumerate(data)]
            if self.marker_channel:
                data_write.append(str(self.current_event))
                self.clear_event(current_event_cnt)
            data_write.append(str(time_ns()))
            self.log.write(",".join(data_write) + "\n")
            # self.log.flush()
            # sleep(self._read_period_seconds)
            if self.profiling_mode:
                print("Total:")
                print(time_ns() - start)
                print("--")


    def terminate(self):
        self.log.flush()
        super().terminate()
        self.serial.close()

    def _read_emg_once(self):
        # start = time_ns()
        line = self.reader.readline()
        # line = self.serial.readline()
        # elapsed_read = time_ns() - start
        # print("Elapsed read:")
        # print(elapsed_read)
        # print(line)
        
        raw = line.decode('utf-8')

        # print(f"Raw: {raw}")

        # Parse the line to extract the signal value
        signal = raw.replace('\n', '').replace('\r', '').split(",")
        # signal_search = re.search(self._sig_entries_regexp, line.decode('utf-8'))
        return signal